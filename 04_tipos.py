#Tipos de datos en Python

print(type(10))                  # Int
print(type(3.14))                # Float
print(type(1 + 3j))              # Complex
print(type('cadena de caracteres'))          # String
print(type([1, 2, 3]))           # List
print(type({'name':'nombre'}))   # Dictionary
print(type({9.8, 3.14, 2.7}))    # Set
print(type((9.8, 3.14, 2.7)))    # Tuple